/* 
 * Ariel Todoki and Sophia Anderson (worked together on Disk)
 * Lab #2
 * Due: September 13, 2017
 */
/*
    Normals code added by Daniel Koenig and Scotti Anderson
    Orr Lab #5
*/
/*
    Texture Coordinates added by Daniel Koenig 
    Orr Lab #7
*/

///// DISK DEFINTION
/////
///// Disk is defined to be centered at the origin of the coordinate reference system. 
///// Disk size is assumed to be r=1, center (0,0,0).

///// Disk is lying in the XY plane
///// slice variable indicates the number of "pizza slices" for the cylinder faces

///// Always use the Right Hand Rule to generate vertex sequence. We want outward facing normals.

//x is user input for the number of slices
function Disk(x) {
    var slice = x;
    
    this.name = "disk";

    this.numTriangles = slice;
    this.numVertices = 3*this.numTriangles;
    this.radius = 1;

    this.vertices = [];
    this.colors = [];
    this.normals = [];
    this.texCoords = [];
    this.tangents = [];
    this.bitangents = [];

    this.ka = 0.2;
    this.kd = 1.0;
    this.ks = 0.8;
    this.shininess = 50.0;
    
    //Declare angle per slice in radians:
    var angle = (2*Math.PI)/this.numTriangles;
    
    //Push all vertices and colors to corresponding arrays:
    for (var i = 0; i < this.numTriangles; i++){
        
        //Create vertices: 
        this.vertices.push(vec4(0,0,0,1));
        this.vertices.push(vec4(Math.cos(angle * i) * this.radius, Math.sin(angle * i) * this.radius, 0, 1));
        this.vertices.push(vec4(Math.cos(angle * (i + 1)) * this.radius, Math.sin(angle * (i + 1)) * this.radius, 0, 1));
        
        //Create colors:
        this.colors.push(vec4(1.0, 0.0, 1.0, 1.0)); //magenta
        this.colors.push(vec4(1.0, 1.0, 1.0, 1.0)); //white
        this.colors.push(vec4(1.0, 0.0, 0.0, 1.0)); //red

        //Create normals: 
        this.normals.push(vec4(0, 0, 1, 0));
        this.normals.push(vec4(0, 0, 1, 0));
        this.normals.push(vec4(0, 0, 1, 0));

        //Create texCoords:
        this.texCoords.push(vec2(this.radius / 2, this.radius / 2));
        this.texCoords.push(vec2((((Math.cos(angle * i) * this.radius) + this.radius) / 2),
            (((Math.sin(angle * i) * this.radius) + this.radius) / 2)));
        this.texCoords.push((vec2((((Math.cos(angle * (i + 1)) * this.radius) + this.radius) / 2),
            (((Math.sin(angle * (i + 1)) * this.radius) + this.radius)) / 2)));

        
    }

    for(var i = 0; i < this.vertices.length; i +=3)
    {
        //Create tangents and bitangents: 
        var Q1 = subtract(this.vertices[i + 1], this.vertices[i]);
        var Q2 = subtract(this.vertices[i + 2], this.vertices[i]);
        var UV1 = subtract(this.texCoords[i + 1], this.texCoords[i]);
        var UV2 = subtract(this.texCoords[i + 2], this.texCoords[i]);

        inverseSTMatrixValue = 1 / ((UV1[0] * UV2[1]) - (UV2[0] * UV1[1]));
        if (inverseSTMatrixValue == Infinity) {
            inverseSTMatrixValue = .001;
        }

        console.log(new mat3(
                    new vec3(UV2[1], -UV1[1], 0),
                    new vec3(-UV2[0], UV1[0], 0),
                    new vec3(0, 0, 0)
                ));
        console.log(new mat3(
                    new vec3(Q1[0], Q1[1], Q1[2]),
                    new vec3(Q2[0], Q2[1], Q2[2]),
                    new vec3(0, 0, 0)
                ));
        console.log(mult(
                new mat3(
                    new vec3(UV2[1], -UV1[1], 0),
                    new vec3(-UV2[0], UV1[0], 0),
                    new vec3(0, 0, 0)
                ),
                new mat3(
                    new vec3(Q1[0], Q1[1], Q1[2]),
                    new vec3(Q2[0], Q2[1], Q2[2]),
                    new vec3(0, 0, 0)
                )));
        var tangentBitangentMatrixVert1 =
            mult(
                new mat3(
                    new vec3(UV2[1], -UV1[1], 0),
                    new vec3(-UV2[0], UV1[0], 0),
                    new vec3(0, 0, 0)
                ),
                new mat3(
                    new vec3(Q1[0], Q1[1], Q1[2]),
                    new vec3(Q2[0], Q2[1], Q2[2]),
                    new vec3(0, 0, 0)
                )
            );
        for (var k = 0; i < 3; i++) {
            for (var j = 0; j < 3; j++) {
                tangentBitangentMatrixVert1[k][j] = tangentBitangentMatrixVert1[k][j] * inverseSTMatrixValue
            }
        }

        tangent1 = tangentBitangentMatrixVert1[0];
        bitangent1 = tangentBitangentMatrixVert1[1];

        normal = this.normals[i];
        //tangent1Prime = subtract(tangent1, mult(normal, (dot(normal, tangent1))));
        //bitangent1Prime = subtract(subtract(bitangent1, mult(normal, dot(normal, bitangent1))),
        //    mult(tangent1Prime, dot(tangent1Prime, bitangent1)));

    }
}

