/* 
 * Ariel Todoki and Sophia Anderson (Sophia independently wrote Cone.js)
 * Lab #2
 * Due: September 13, 2017
 */
/*
    Normals code added by Daniel Koenig and Scotti Anderson
    Orr Lab #5
*/
/*
    Texture Coordinates added by Daniel Koenig 
    Orr Lab #7
*/

//x is user input for the number of slices
function Cone(x) {
    var slices = x;

    this.name = "Cone"; //shape name

    this.numTriangles = slices * 2; // has twice the triangles of a disk
    this.numVertices = this.numTriangles * 3; //three verticies per triangle
    this.radius = 1;
    this.height = 1;

    this.vertices = [];
    this.colors = [];
    this.normals = [];
    this.texCoords = [];

    for (var i = 0; i < slices; i++)
    {
        var angle = (2 * Math.PI) / slices; //angle of the triangle
        
        ////Create vertices: 
        //Create the vertices for the center, tip, and triangle edges: 
        var center = vec4(0, 0, 0, 1.0);
        var point1 = vec4(Math.cos(i * angle), 0, Math.sin(i * angle), 1.0);
        var point2 = vec4(Math.cos((i + 1) * angle), 0, Math.sin((i + 1) * angle), 1.0);
        var tip = vec4(0, this.height, 0, 1.0);

        //Push vertices for bottom of cone: 
        this.vertices.push(center);
        this.vertices.push(point1);
        this.vertices.push(point2);

        ///Push vertices for the triangle going to tip:
        this.vertices.push(tip);
        this.vertices.push(point1);
        this.vertices.push(point2);

        //Create colors:
        //Push colors for bottom of cone:
        this.colors.push(vec4(1.0, 0, 0, 1.0));
        this.colors.push(vec4(1, .5, 0, 1.0));
        this.colors.push(vec4(1.0, 0, .5, 1.0));
        
        //Push colors for side of cone:
        this.colors.push(vec4(1.0, 0, 0, 1.0));
        this.colors.push(vec4(1, .5, 0, 1.0));
        this.colors.push(vec4(1.0, 0, .5, 1.0));

        ////Create normals: 
        //Calculate normal for each face by making a vector whose x & z direction is oriented towards point1's x & z 
        //and whose y direction is oriented up at the reciprocal of the cone's side: 
        var faceNormal = normalize(vec4(point1[0], this.radius / this.height, point1[2], 0));

        //Create normals for bottom of cone: 
        this.normals.push(vec4(0, -1, 0, 0));
        this.normals.push(vec4(0, -1, 0, 0));
        this.normals.push(vec4(0, -1, 0, 0));

        //Create normals for side of cone: 
        this.normals.push(faceNormal);
        this.normals.push(faceNormal);
        this.normals.push(faceNormal);

        ////Create texCoords: 
        convertRadiansToDegrees = 180 / Math.PI;
        //Push texCoords for bottom of cone: 
        this.texCoords.push(vec2(this.radius / 2, this.radius / 2)); //center
        this.texCoords.push(vec2((((Math.cos(angle * i) * this.radius) + this.radius) / 2),
            (((Math.sin(angle * i) * this.radius) + this.radius) / 2))); //p1
        this.texCoords.push((vec2((((Math.cos(angle * (i + 1)) * this.radius) + this.radius) / 2),
            (((Math.sin(angle * (i + 1)) * this.radius) + this.radius)) / 2))); //p2

        //Push texCoords for side of cone: 
        this.texCoords.push(vec2(this.radius / 2, this.radius / 2)); //tip
        this.texCoords.push(vec2((((Math.cos(angle * i) * this.radius) + this.radius) / 2),
            (((Math.sin(angle * i) * this.radius) + this.radius) / 2))); //p1
        this.texCoords.push((vec2((((Math.cos(angle * (i + 1)) * this.radius) + this.radius) / 2),
            (((Math.sin(angle * (i + 1)) * this.radius) + this.radius)) / 2))); //p2
    }
}


