/** Build and Draw Primitive Shapes
 * 
 * Global Variables:
 *   gl: the webgl graphics context.
 *   cameraProgram.attribs.vPosition: shader variable location for vertex position attribute in camera shader
 *   shadowMapGenProgram.attribs.vPosition: shader variable location for vertex position attribute in shadow shadow
 *   cameraProgram.attribs.vColor: shader variable location for color attribute
 *   cameraProgram.attribs.vNormal: shader variable location for normal attribute 
 *   cameraProgram.attribs.vTexCoord: shader variable location for texture coordinate attribute
 */

var Shapes = {};   // set up Shapes namespace

Shapes.cube = new Cube();  
Shapes.axis = new Axis();
Shapes.cylinder = new Cylinder(12);
Shapes.disk = new Disk(12);
Shapes.cone = new Cone(12);


Shapes.initShapes = function () {
    Shapes.initBuffers(Shapes.cube);
    Shapes.initBuffers(Shapes.cylinder);
    Shapes.initBuffers(Shapes.disk);
    Shapes.initBuffers(Shapes.cone);
    Shapes.axis.initBuffer();
};


Shapes.initBuffers = function (primitive) {

    // SET UP ARRAY BUFFER FOR VERTICES 
    ////////////////////////////////////////////////////////////
    primitive.vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, primitive.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(primitive.vertices), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null); // done with this buffer

    // SET UP ARRAY BUFFER FOR VERTEX COLORS 
    ////////////////////////////////////////////////////////////
    primitive.colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, primitive.colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(primitive.colors), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null); // done with this buffer

    // SET UP ARRAY BUFFER FOR VERTEX NORMALS 
    ////////////////////////////////////////////////////////////
    primitive.normalBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, primitive.normalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(primitive.normals), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null); // done with this buffer

    // SET UP ARRAY BUFFER FOR VERTEX TEXCOORDS 
    ////////////////////////////////////////////////////////////
    primitive.texBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, primitive.texBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(primitive.texCoords), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null); // done with this buffer
}

Shapes.drawPrimitive = function (primitive) {
    gl.bindBuffer(gl.ARRAY_BUFFER, primitive.vertexBuffer);
    //shadow program attribute information: 
    gl.enableVertexAttribArray(shadowMapGenProgram.attribs.vPosition);
    gl.vertexAttribPointer(shadowMapGenProgram.attribs.vPosition, 4, gl.FLOAT, false, 0, 0);
    //camera program attribute information:
    gl.enableVertexAttribArray(cameraProgram.attribs.vPosition);
    gl.vertexAttribPointer(cameraProgram.attribs.vPosition, 4, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, primitive.colorBuffer);
    gl.enableVertexAttribArray(cameraProgram.attribs.vColor);
    gl.vertexAttribPointer(cameraProgram.attribs.vColor, 4, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, primitive.normalBuffer);
    gl.enableVertexAttribArray(cameraProgram.attribs.vNormal);
    gl.vertexAttribPointer(cameraProgram.attribs.vNormal, 4, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, primitive.texBuffer);
    gl.enableVertexAttribArray(cameraProgram.attribs.vTexCoord);
    gl.vertexAttribPointer(cameraProgram.attribs.vTexCoord, 2, gl.FLOAT, false, 0, 0);

    gl.drawArrays(gl.TRIANGLES, 0, primitive.numVertices);

    gl.disableVertexAttribArray(cameraProgram.attribs.vPosition);
    gl.disableVertexAttribArray(cameraProgram.attribs.vColor);
    gl.disableVertexAttribArray(cameraProgram.attribs.vNormal);
    gl.disableVertexAttribArray(cameraProgram.attribs.vTexCoord);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
};


