/*
    Shadow mapping and normal mapping added by Daniel Koenig
    Orr Final
    12.14.2017
*/

var canvas;       // HTML 5 canvas
var gl;           // webgl graphics context
var cameraProgram;      // camera program object for WebGL
var shadowMapGenProgram;    //shadow program object for WebGL
var lightOne;     // lighting object
var me = this;  
var camera = new Camera(vec4(0, 4, 25, 1), vec4([0, 0, 1, 0]), vec4([0, 1, 0, 0]), true);
var stack = new MatrixStack();

window.onload = function init() {
    //set Event Handlers
    setKeyEventHandler();
    setMouseEventHandler();

    me.textureSize = 2048;

    canvas = document.getElementById("gl-canvas");

    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) { alert("WebGL isn't available"); }

    gl.enable(gl.DEPTH_TEST);

    shaderSetup();

    Shapes.initShapes();  // create the primitives and other shapes       

    render();
};

/**
 *  Load shaders, attach shaders to programs, obtain handles for 
 *  the attribute and uniform variables.
 * @return {undefined}
 */
function shaderSetup() {
    //  Load shaders:
    cameraProgram = initShaders(gl, "camera-vertex-shader", "camera-fragment-shader");
    shadowMapGenProgram = initShaders(gl, "shadow-map-gen-vertex-shader", "shadow-map-gen-fragment-shader");
    gl.linkProgram(cameraProgram);
    gl.linkProgram(shadowMapGenProgram);

    lightOne = new Lighting();
    gl.useProgram(cameraProgram);
    lightOne.setUp();

    //me.normalMapTexture = new ImageTexture("textures/emboss-normalmap.png");
    me.normalMapTexture = new ImageTexture("textures/normal.png");


    cameraProgram.attribs = {
        vPosition: gl.getAttribLocation(cameraProgram, "vPosition"),
        vColor: gl.getAttribLocation(cameraProgram, "vColor"),
        vNormal: gl.getAttribLocation(cameraProgram, "vNormal"),
        vTexCoord: gl.getAttribLocation(cameraProgram, "vTexCoord"),
    };

    cameraProgram.uniforms = {
        uColor: gl.getUniformLocation(cameraProgram, "uColor"),
        uColorTouse: gl.getUniformLocation(cameraProgram, "uColorToUse"),
        uTexture: gl.getUniformLocation(cameraProgram, "uTexture"),
        uProjection: gl.getUniformLocation(cameraProgram, "uProjection"),
        uModel_view: gl.getUniformLocation(cameraProgram, "uModel_view"),
        uShadowProjection: gl.getUniformLocation(cameraProgram, "uShadowProjection"),
        uShadowModel_view: gl.getUniformLocation(cameraProgram, "uShadowModel_view"),
        uLight_position: gl.getUniformLocation(cameraProgram, "uLight_position"),
        uShadowMap: gl.getUniformLocation(cameraProgram, "uShadowMap"),
        uNormalMap: gl.getUniformLocation(cameraProgram, "uNormalMap"),
    };

    shadowMapGenProgram.attribs = {
        vPosition: gl.getAttribLocation(shadowMapGenProgram, 'vPosition'),
    };

    shadowMapGenProgram.uniforms = {
        uProjection: gl.getUniformLocation(shadowMapGenProgram, 'uProjection'),
        uModel_view: gl.getUniformLocation(shadowMapGenProgram, 'uModel_view'),
    };

    setUpShadowMap();
}

function setUpShadowMap() {
    gl.useProgram(shadowMapGenProgram);
    me.floatExtension = me.gl.getExtension("OES_texture_float");
    me.floatLinearExtension = me.gl.getExtension("OES_texture_float_linear");

    //set up shadowMapTexture:
    me.shadowMapTexture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, me.shadowMapTexture);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.MIRRORED_REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.MIRRORED_REPEAT);
    if (me.floatExtension && me.floatLinearExtension){
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, me.textureSize, me.textureSize, 0, gl.RGBA, gl.FLOAT, null);
        console.log("Extensions loaded properly! Using FLOAT data type.");
    }
    else {
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, me.textureSize, me.textureSize, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
        console.log("Extensions failed to load! Using UNSIGNED_BYTE data type.");
    }

    me.shadowMapTextureFramebuffer = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, me.shadowMapTextureFramebuffer);

    me.shadowTextureRenderbuffer = gl.createRenderbuffer();
    gl.bindRenderbuffer(gl.RENDERBUFFER, me.shadowTextureRenderbuffer);
    gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, me.textureSize, me.textureSize);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);
    gl.bindTexture(gl.TEXTURE_2D, null);

    me.shadowMapProj = ortho(-40, 40, -40, 40, -40.0, 80);
}

function render() {
    generateShadowMap();
    drawModels();
}

function generateShadowMap() {
    gl.useProgram(shadowMapGenProgram);

    gl.bindTexture(gl.TEXTURE_2D, me.shadowMapTexture);
    gl.bindFramebuffer(gl.FRAMEBUFFER, me.shadowMapTextureFramebuffer);
    gl.bindRenderbuffer(gl.RENDERBUFFER, me.shadowTextureRenderbuffer);

    gl.viewport(0, 0, me.textureSize, me.textureSize);
    gl.enable(gl.DEPTH_TEST);
    //gl.enable(gl.CULL_FACE);

    gl.uniformMatrix4fv(shadowMapGenProgram.uniforms.uProjection, gl.FALSE, flatten(me.shadowMapProj));
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, me.shadowMapTexture, 0);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, me.shadowTextureRenderbuffer);
    gl.clearColor(0, 0, 0, 1);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    stack.clear();
    stack.multiply(me.lightOne.calcViewMat());
    me.shadowMatrices = [];

    //draw middle cube:
    stack.push();
    stack.multiply(translate(0, 4, 0));
    stack.multiply(scalem(2, 2, 2));
    gl.uniformMatrix4fv(shadowMapGenProgram.uniforms.uModel_view, false, flatten(stack.top()));
    me.shadowMatrices[0] = stack.top();
    Shapes.drawPrimitive(Shapes.cube);
    stack.pop();

    //draw bottom floor:
    stack.push();
    stack.multiply(translate(0, -.1, 0));
    stack.multiply(scalem(10, .25, 10));
    gl.uniformMatrix4fv(shadowMapGenProgram.uniforms.uModel_view, false, flatten(stack.top()));
    me.shadowMatrices[1] = stack.top();
    Shapes.drawPrimitive(Shapes.cube);
    stack.pop();

    //draw cylinder one:
    stack.push();
    stack.multiply(translate(-4, 4, -4));
    stack.multiply(scalem(2, 2, 2));
    gl.uniformMatrix4fv(shadowMapGenProgram.uniforms.uModel_view, false, flatten(stack.top()));
    me.shadowMatrices[2] = stack.top();
    Shapes.drawPrimitive(Shapes.cylinder);
    stack.pop();

    //draw cylinder two:
    stack.push();
    stack.multiply(translate(4, 4, 4));
    stack.multiply(scalem(2, 2, 2));
    gl.uniformMatrix4fv(shadowMapGenProgram.uniforms.uModel_view, false, flatten(stack.top()));
    me.shadowMatrices[3] = stack.top();
    Shapes.drawPrimitive(Shapes.cylinder);
    stack.pop();

    //draw cone one: 
    stack.push();
    stack.multiply(translate(-4, 4, 4));
    stack.multiply(scalem(2, 2, 2));
    gl.uniformMatrix4fv(shadowMapGenProgram.uniforms.uModel_view, false, flatten(stack.top()));
    me.shadowMatrices[4] = stack.top();
    Shapes.drawPrimitive(Shapes.cone);
    stack.pop();

    //draw cone two: 
    stack.push();
    stack.multiply(translate(4, 4, -4));
    stack.multiply(scalem(2, 2, 2));
    gl.uniformMatrix4fv(shadowMapGenProgram.uniforms.uModel_view, false, flatten(stack.top()));
    me.shadowMatrices[5] = stack.top();
    Shapes.drawPrimitive(Shapes.cone);
    stack.pop();

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);
    gl.bindTexture(gl.TEXTURE_2D, null);
};

function drawModels() {
    gl.useProgram(cameraProgram);

    //gl.enable(gl.CULL_FACE);
    gl.enable(gl.DEPTH_TEST);

    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.98, 0.98, 0.98, 1);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    projMat = camera.calcProjectionMat(); 
    viewMat = camera.calcViewMat();   
    gl.uniformMatrix4fv(cameraProgram.uniforms.uProjection, false, flatten(projMat));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uShadowProjection, gl.FALSE, flatten(me.shadowMapProj));

    //transform the light's position into CCS from WCS, then set the uniform lighting variable:
    gl.uniform4fv(cameraProgram.uniforms.uLight_position, mult(viewMat, me.lightOne.light_position));

    gl.uniform1i(cameraProgram.uniforms.uShadowMap, 0);
    gl.bindTexture(gl.TEXTURE_2D, me.shadowMapTexture);
    gl.uniform1i(cameraProgram.uniforms.uNormalMap, 2);
    me.normalMapTexture.activate();

    stack.clear();
    stack.multiply(viewMat);

    //draw middle cube:
    stack.push();
    stack.multiply(translate(0, 4, 0));
    stack.multiply(scalem(2, 2, 2));
    gl.uniform4fv(cameraProgram.uniforms.uColor, vec4(.5, 0, 0, 1)); 
    gl.uniformMatrix4fv(cameraProgram.uniforms.uModel_view, false, flatten(stack.top()));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uShadowModel_view, false, flatten(me.shadowMatrices[0]));
    Shapes.drawPrimitive(Shapes.cube);
    stack.pop();

    //draw bottom floor:
    stack.push();
    stack.multiply(translate(0, -.1, 0));
    stack.multiply(scalem(10, .25, 10));
    gl.uniform4fv(cameraProgram.uniforms.uColor, vec4(.8, .2, .3, 1));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uModel_view, false, flatten(stack.top()));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uShadowModel_view, false, flatten(me.shadowMatrices[1]));
    Shapes.drawPrimitive(Shapes.cube);
    stack.pop();

    //draw cylinder one:
    stack.push();
    stack.multiply(translate(-4, 4, -4));
    stack.multiply(scalem(2, 2, 2));
    gl.uniform4fv(cameraProgram.uniforms.uColor, vec4(.7, .4, 0.0, 1));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uModel_view, false, flatten(stack.top()));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uShadowModel_view, false, flatten(me.shadowMatrices[2]));
    Shapes.drawPrimitive(Shapes.cylinder);
    stack.pop();

    //draw cylinder two:
    stack.push();
    stack.multiply(translate(4, 4, 4));
    stack.multiply(scalem(2, 2, 2));
    gl.uniform4fv(cameraProgram.uniforms.uColor, vec4(.87, .84, .74, 1));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uModel_view, false, flatten(stack.top()));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uShadowModel_view, false, flatten(me.shadowMatrices[3]));
    Shapes.drawPrimitive(Shapes.cylinder);
    stack.pop();

    //draw cone one:
    stack.push();
    stack.multiply(translate(-4, 4, 4));
    stack.multiply(scalem(2, 2, 2));
    gl.uniform4fv(cameraProgram.uniforms.uColor, vec4(.24, .78, .12, 1));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uModel_view, false, flatten(stack.top()));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uShadowModel_view, false, flatten(me.shadowMatrices[4]));
    Shapes.drawPrimitive(Shapes.cone);
    stack.pop();

    //draw cone two:
    stack.push();
    stack.multiply(translate(4, 4, -4));
    stack.multiply(scalem(2, 2, 2));
    gl.uniform4fv(cameraProgram.uniforms.uColor, vec4(.5, .4, .9, 1));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uModel_view, false, flatten(stack.top()));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uShadowModel_view, false, flatten(me.shadowMatrices[5]));
    Shapes.drawPrimitive(Shapes.cone);
    stack.pop();

    //draw LightCube
    stack.push();
    stack.multiply(translate(lightOne.light_position[0], lightOne.light_position[1], lightOne.light_position[2]));
    stack.multiply(scalem(0.1, 0.1, 0.1));
    gl.uniformMatrix4fv(cameraProgram.uniforms.uModel_view, false, flatten(stack.top()));
    gl.uniform4fv(cameraProgram.uniforms.uColor, vec4(0.0, 0.0, 0.0, 1));  // set color to black
    Shapes.drawPrimitive(Shapes.cube);
    stack.pop();
}

